const express = require('express')
const router = express.Router()
const Album = require('../models/Album')

// Middleware que converte o body para um objeto
router.use(express.json())
// router.use(express.urlencoded())

// Retorna todos os álbuns
router.get('/albuns', async (req, res) => {
  try {
    let albuns = []
    albuns = await Album.find().populate('artista')

    res.json(albuns)
  } catch (err) {
    console.error(err.message)
    res.status(500).json({ erro: 'erro' })
  }
})

// Retorna apenas um album baseado no ID
router.get('/albuns/:id', async (req, res) => {

  try {
    const id = req.params.id
    let album = await Album.findById(id)
    res.json(album)
  } catch (err) {
    console.error(err.message)
    res.json({ error: "id não foi encontrado" })
  }
})

// Salva um Objeto enviado através do body
router.post('/albuns', async (req, res) => {
  try {
    const album = new Album(req.body)
    const resultado = await album.save()
    res.json(resultado)
  } catch (error) {
    console.error(err.message)
    res.json({ error: "erro ao salvar o album" })
  }
})

// Modifica uma parte do album de acordo com o ID e o body
router.put('/albuns/:id', async (req, res) => {
  try {
    const id = req.params.id
    let albumBody = req.body
    const resultado = await Album.findOneAndUpdate(id, albumBody)
    res.json(resultado)
  } catch (err) {
    console.error(err.message)
    res.json({ error: "erro ao atualizar, verifica as informações." })
  }
})

// delete o album de acordo com o ID 
router.delete('/albuns/:id', async (req, res) => {
  try {
    const id = req.params.id
    const resultado = await Album.findOneAndDelete(id)
    res.json(resultado)
  } catch (err) {
    console.error(err.message)
    res.json({ error: "Erro." })
  }
})


module.exports = router